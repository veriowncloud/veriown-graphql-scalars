Installation

```
npm install --save @veriown/graphql-scalars
```

Usage

1. Import the types in your resolver-map.

```
const {  DateTime, Date, Coordinates, Url, PhoneNumber, Email } =  require('@veriown/graphql-scalars');
```

2. Declare the types in your schema file

```
		scalar Date
		scalar DateTime
		scalar Url
		scalar Coordinates
		scalar PhoneNumber
		scalar Email
		scalar RelativeUrl
		scalar Uuid
```	

3. Use them like regular GraphQL types
```    
        type User {
          id: ID!
          name: String
          address: String
          houseCoordinates: Coordinates!
          phone: PhoneNumber
          website: [Url]
          dob: Date
          email: Email
        }
```        
        
        
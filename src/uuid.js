const { GraphQLScalarType } = require('graphql');
const { GraphQLError } = require('graphql/error');
const { Kind } = require('graphql/language');
const { isUUID } = require('validator');

const validateUUID = (input) => {
  if (typeof input !== 'string') {
    throw new TypeError(`InvalidType: ${input} must be a string`);
  }

  if (!isUUID(input)) {
    throw new TypeError(`InvalidUUID: ${input} is not a valid UUID`);
  }
};

module.exports = new GraphQLScalarType({
  name: 'UUID',

  description: 'A UUID string',

  serialize(value) {
    validateUUID(value);

    return value;
  },

  parseValue(value) {
    validateUUID(value);

    return value;
  },

  parseLiteral(ast) {
    if (ast.kind !== Kind.STRING) {
      throw new GraphQLError(`Can only validate strings as IDs but got a: ${ast.kind}`);
    }

    validateUUID(ast.value);

    return ast.value;
  }
});

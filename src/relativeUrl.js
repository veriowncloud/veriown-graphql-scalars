const { GraphQLScalarType } = require('graphql');
const { GraphQLError } = require('graphql/error');
const { Kind } = require('graphql/language');
const { isURL } = require('validator');

const validateRelativeUrl = (url) => {
  if (typeof url !== 'string') {
    throw new TypeError(`InvalidType: ${url} must be a string`);
  }

  // reject absoulte urls
  if (isURL(url, { require_host: true })) {
    throw new TypeError(`InvalidRelativeURL: ${url} is an absolute URL`);
  }

  if (!isURL(url, { require_host: false })) {
    throw new TypeError(`InvalidRelativeURL: ${url} is not a valid URL`);
  }
};

module.exports = new GraphQLScalarType({
  name: 'RelativeURL',

  description: 'A valid relative URL (a URL without a host/domain)',

  serialize(value) {
    validateRelativeUrl(value);

    return value;
  },

  parseValue(value) {
    validateRelativeUrl(value);

    return value;
  },

  parseLiteral(ast) {
    if (ast.kind !== Kind.STRING) {
      throw new GraphQLError(`Can only validate strings as relativeURLs but got a: ${ast.kind}`);
    }

    validateRelativeUrl(ast.value);

    return ast.value;
  }
});

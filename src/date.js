const { GraphQLScalarType } = require('graphql');
const { GraphQLError } = require('graphql/error');
const { Kind } = require('graphql/language');
const moment = require('moment');

const formats = ['YYYY-MM-DD', 'DD-MM-YYYY'];
module.exports = new GraphQLScalarType({
  name: 'Date',

  description: 'A valid Javascript date in format YYYY-MM-DD or DD-MM-YYYY.',

  serialize(value) {
    if (!(value instanceof Date)) {
      throw new TypeError(`Value is not an instance of date: ${value}`);
    }

    if (Number.isNaN(value.getTime())) {
      throw new TypeError(`Value is not a valid date: ${value}`);
    }

    return moment(value).format('YYYY-MM-DD');
  },

  parseValue(value) {
    const date = moment(value);

    if (!date.isValid()) {
      throw new TypeError(`Value is not a valid date: ${value}`);
    }

    return date.format('YYYY-MM-DD');
  },

  parseLiteral(ast) {
    if (ast.kind !== Kind.STRING) {
      throw new GraphQLError(`Can only parse strings to dates but got a: ${ast.kind}`);
    }

    if (!moment(ast.value, formats).isValid()) {
      throw new TypeError(`${ast.value} is not a valid date format.`);
    }

    const date = moment(ast.value);

    if (!date.isValid()) {
      throw new GraphQLError(`Value is not a valid date: ${ast.value}`);
    }

    return date.format('YYYY-MM-DD');
  }
});

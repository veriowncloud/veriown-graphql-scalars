const { describe, it } = require('mocha');
const chai = require('chai');
const { Kind } = require('graphql/language');
const { GraphQLError } = require('graphql/error');
const url = require('./relativeUrl');

const { expect } = chai;

describe('GraphqlScalar: RelativeURL', () => {
  describe('.serialize', () => {
    it('should validate a correct relativeURL ', () => {
      const imgURL = '/u/6735915?s=400&u=dd0a9abd499efc6316aa069464a9f147ca3e9171&v=4';
      expect(url.serialize(imgURL)).to.equal(imgURL);
    });

    it('should fail for invalid relativeUrl', () => {
      expect(url.serialize.bind(url, 'xyz')).to.throw('not a valid URL');
    });

    it('should fail for the absolute URL', () => {
      const testURL = 'https://avatars1.githubusercontent.com/u/6735915?s=400&u=dd0a9abd499efc6316aa069464a9f147ca3e9171&v=4';
      expect(url.serialize.bind(url, testURL)).to.throw('is an absolute URL');
    });
  });

  describe('.parseValue', () => {
    it('should validate a correct relativeURL ', () => {
      const imgURL = '/u/6735915?s=400&u=dd0a9abd499efc6316aa069464a9f147ca3e9171&v=4';
      expect(url.parseValue(imgURL)).to.equal(imgURL);
    });

    it('should fail for invalid relativeUrl', () => {
      expect(url.parseValue.bind(url, 'xyz')).to.throw('is not a valid URL');
    });

    it('should fail for the absolute URL', () => {
      const testURL = 'https://avatars1.githubusercontent.com/u/6735915?s=400&u=dd0a9abd499efc6316aa069464a9f147ca3e9171&v=4';
      expect(url.parseValue.bind(url, testURL)).to.throw('is an absolute URL');
    });
  });

  describe('.parseLiteral', () => {
    it('should parse literal', () => {
      const testURL = '/u/6735915?s=400&u=dd0a9abd499efc6316aa069464a9f147ca3e9171&v=4';
      expect(url.parseLiteral({
        value: testURL,
        kind: Kind.STRING
      })).to.equal(testURL);
    });

    it('should fail if parse  literal kind is not correct', () => {
      const testURL = '/u/6735915?s=400&u=dd0a9abd499efc6316aa069464a9f147ca3e9171&v=4';
      expect(url.parseLiteral.bind(url, {
        value: testURL,
        kind: Kind.INT
      })).to.throw(GraphQLError);
    });
  });
});

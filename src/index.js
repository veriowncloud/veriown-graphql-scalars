const Url = require('./url');
const DateTime = require('./dateTime');
const Date = require('./date');
const Coordinates = require('./coordinates');
const PhoneNumber = require('./phoneNumber');
const Email = require('./email');
const RelativeUrl = require('./relativeUrl');
const Uuid = require('./uuid');

module.exports = {
  Url,
  DateTime,
  Date,
  Coordinates,
  PhoneNumber,
  Email,
  RelativeUrl,
  Uuid
};

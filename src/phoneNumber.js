const { GraphQLScalarType } = require('graphql');
const { GraphQLError } = require('graphql/error');
const { Kind } = require('graphql/language');

const PHONE_NUMBER_REGEX = new RegExp(/^\+\d{11,15}$/);
module.exports = new GraphQLScalarType({
  name: 'PhoneNumber',

  description:
    'A field whose value conforms to the standard E.164 format.(https://en.wikipedia.org/wiki/E.164)',

  serialize(value) {
    if (typeof value !== 'object') {
      throw new GraphQLError(`Value is not object: ${value}`);
    }

    if (!value.countryCode || !value.number) {
      throw new GraphQLError(`Value is not valid phone number: ${value}`);
    }

    const phone = `${value.countryCode}${value.number}`;

    if (!PHONE_NUMBER_REGEX.test(phone)) {
      throw new GraphQLError(`Value is not a valid phone number of the form +17895551234 (10-15 digits): ${value}`);
    }

    return value;
  },

  parseValue(value) {
    if (typeof value !== 'object') {
      throw new TypeError(`Value is not object: ${value}`);
    }

    if (!value.countryCode || !value.number) {
      throw new TypeError(`Value is not valid phone number: ${value}`);
    }

    const phone = `${value.countryCode}${value.number}`;

    if (!PHONE_NUMBER_REGEX.test(phone)) {
      throw new TypeError(`Value is not a valid phone number of the form +17895551234 (10-15 digits): ${value}`);
    }

    return value;
  },

  parseLiteral(ast) {
    if (ast.kind !== Kind.OBJECT) {
      throw new GraphQLError(`Can only parse objects to phone but got a: ${ast.kind}`);
    }

    const phone = {};

    ast.fields.forEach((field) => {
      const { value: name } = field.name;
      const { value } = field.value;

      phone[name] = value;
    });

    const phoneNumber = `${phone.countryCode}${phone.number}`;

    if (!PHONE_NUMBER_REGEX.test(phoneNumber)) {
      throw new TypeError(`Value is not a valid phone number of the form +17895551234 (10-15 digits): ${phone}`);
    }

    return phone;
  }
});

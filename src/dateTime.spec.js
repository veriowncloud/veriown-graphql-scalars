const { describe, it } = require('mocha');
const chai = require('chai');
const { Kind } = require('graphql/language');
const datetime = require('./dateTime');

const { expect } = chai;

describe('DateTime', () => {
  describe('validate datetime', () => {
    it('should be a valid date', async () => {
      const now = new Date();
      expect(datetime.serialize(now)).to.be.equal(now.toJSON());
    });

    it('should parse date value', () => {
      const now = new Date();
      expect(datetime.parseValue(now)).to.deep.equal(now);
    });
  });

  it('should parse literal', () => {
    const result = new Date(Date.UTC(2017, 0, 2, 3, 4, 5, 0));
    expect(datetime.parseLiteral({
      value: '2017-01-02T03:04:05.000Z',
      kind: Kind.STRING
    })).to.deep.equal(result);
  });
});

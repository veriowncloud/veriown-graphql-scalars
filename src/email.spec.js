const { describe, it } = require('mocha');
const chai = require('chai');
const { Kind } = require('graphql/language');
const { GraphQLError } = require('graphql/error');
const email = require('./email');

const { expect } = chai;

describe('Email', () => {
  describe('.serialize', () => {
    it('should validate a correct email ', () => {
      const emails = [
        'ankit.bahuguna@gmail.com',
        'ANKIT.BAHUGUNA@gmail.com',
        'ankit.bahuguna@gmail.co.in',
        'ankit-bahuguna@yahoomail.in'
      ];
      emails.forEach((mail) => {
        expect(email.serialize(mail)).to.equal(mail);
      });
    });

    it('should fail for invalid email', () => {
      expect(email.serialize.bind(email, 'xyz')).to.throw(GraphQLError);
    });

    it('should be success for email with trailing/preceding whitespaces', () => {
      const testEmail = ' test@yopmail.com ';
      expect(email.serialize(testEmail)).to.equal('test@yopmail.com');
    });
  });

  describe('.parseValue', () => {
    it('should parse value', () => {
      const testEmail = 'test@yopmail.com';
      expect(email.parseValue(testEmail)).to.equal(testEmail);
    });

    it('should fail for invalid email', () => {
      expect(email.parseValue.bind(email, 'xyz')).to.throw(GraphQLError);
    });

    it('should be success for email with trailing/preceding whitespaces', () => {
      const testEmail = ' test@yopmail.com ';
      expect(email.parseValue(testEmail)).to.equal('test@yopmail.com');
    });
  });

  describe('.parseLiteral', () => {
    it('should parse literal', () => {
      const testEmail = 'ankit.bahuguna@gmail.com';
      expect(email.parseLiteral({
        value: testEmail,
        kind: Kind.STRING
      })).to.equal(testEmail);
    });

    it('should fail if parse  literal kind is not correct', () => {
      const testEmail = 'ankit@gmail.com';
      expect(email.parseLiteral.bind(email, {
        value: testEmail,
        kind: Kind.INT
      })).to.throw(GraphQLError);
    });

    it('should be success for email with trailing/preceding whitespaces', () => {
      const testEmail = ' test@yopmail.com ';
      expect(email.parseLiteral({
        value: testEmail,
        kind: Kind.STRING
      })).to.equal('test@yopmail.com');
    });
  });
});

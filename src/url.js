const { GraphQLScalarType } = require('graphql');
const { GraphQLError } = require('graphql/error');
const { Kind } = require('graphql/language');
const { isURL } = require('validator');

module.exports = new GraphQLScalarType({
  name: 'URL',

  description: 'A valid URL.',

  serialize(value) {
    if (typeof value !== 'string') {
      throw new TypeError(`Value is not string: ${value}`);
    }

    if (!isURL(value)) {
      throw new TypeError(`Value is not a valid URL: ${value}`);
    }

    return value;
  },

  parseValue(value) {
    if (typeof value !== 'string') {
      throw new TypeError(`Value is not string: ${value}`);
    }

    if (!isURL(value)) {
      throw new TypeError(`Value is not a valid URL: ${value}`);
    }

    return value;
  },

  parseLiteral(ast) {
    if (ast.kind !== Kind.STRING) {
      throw new GraphQLError(`Can only validate strings as URLs but got a: ${ast.kind}`);
    }

    if (!isURL(ast.value)) {
      throw new TypeError(`Value is not a valid URL: ${ast.value}`);
    }

    return ast.value;
  }
});

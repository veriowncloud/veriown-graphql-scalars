const { describe, it } = require('mocha');
const chai = require('chai');
const { Kind } = require('graphql/language');
const { GraphQLError } = require('graphql/error');
const coordinate = require('./coordinates');

const { expect } = chai;

describe('Coordinates', () => {
  describe('validate coordinates', () => {
    it('should validate correct set of coordinates ', () => {
      const location = { lat: 78.154545, lng: 32.4545 };
      expect(coordinate.serialize(location)).to.deep.equal(location);
    });

    it('should validate correct set of coordinates, when passed as string', () => {
      const location = { lat: '78.154545', lng: '32.4545' };
      expect(coordinate.serialize(location)).to.deep.equal(location);
    });

    it('should fail for invalid coordinates', () => {
      const location = { lat: 778.154545, lng: 332.4545 };
      expect(coordinate.serialize.bind(coordinate, location)).to.throw(GraphQLError);
    });
  });

  it('should parse literal', () => {
    const location = { lat: -89.154545, lng: -65.4545 };

    expect(coordinate.parseLiteral({
      fields: [
        {
          kind: Kind.OBJECT_FIELD,
          name: { kind: Kind.NAME, value: 'lng' },
          value: { kind: Kind.FLOAT, value: location.lng }
        },
        {
          kind: Kind.OBJECT_FIELD,
          name: { kind: Kind.NAME, value: 'lat' },
          value: { kind: Kind.FLOAT, value: location.lat }
        }
      ],
      kind: Kind.OBJECT
    })).to.deep.equal(location);
  });

  it('should fail if parse  literal kind is not correct', () => {
    const location = { lat: 778.154545, lng: 332.4545 };
    expect(coordinate.parseLiteral.bind(coordinate, {
      value: location,
      kind: Kind.FLOAT
    })).to.throw(GraphQLError);
  });
});

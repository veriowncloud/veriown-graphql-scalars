const { describe, it } = require('mocha');
const chai = require('chai');
const moment = require('moment');
const { Kind } = require('graphql/language');
const date = require('./date');

const { expect } = chai;

describe('Date', () => {
  describe('validate date', () => {
    it('should be a valid date', async () => {
      const now = new Date();
      expect(date.serialize(now)).to.be.equal(moment(now).format('YYYY-MM-DD'));
    });

    it('should parse date value', () => {
      const now = new Date();
      expect(date.parseValue(now)).to.deep.equal(moment(now).format('YYYY-MM-DD'));
    });
  });

  it('should parse literal', () => {
    const result = new Date('2017-01-02T03:04:05.000Z');
    expect(date.parseLiteral({
      value: '2017-01-02T03:04:05.000Z',
      kind: Kind.STRING
    })).to.deep.equal(moment(result).format('YYYY-MM-DD'));
  });
});

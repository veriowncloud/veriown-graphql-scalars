const { GraphQLScalarType } = require('graphql');
const { GraphQLError } = require('graphql/error');
const { Kind } = require('graphql/language');

function areLatLongValid(lat, lng) {
  if (Number.isNaN(lat) || Number.isNaN(lng)) {
    return false;
  }

  if (lat > 90 || lat < -90) {
    return false;
  }
  if (lng > 180 || lng < -180) {
    return false;
  }

  return true;
}
function isValidCoordinateObject(value) {
  if (value !== Object(value)) {
    return false;
  }
  if (Object.keys(value).length !== 2) {
    return false;
  }
  if (!['lat', 'lng'].every((key) => Object.prototype.hasOwnProperty.call(value, key))) {
    return false;
  }

  const { lat, lng } = value;

  return areLatLongValid(parseFloat(lat), parseFloat(lng));
}

module.exports = new GraphQLScalarType({
  name: 'Coordinates',

  description:
    'A JSON objection representing a coordinates point. Has lat and lng keys representing latitude and longitude of the point.',

  serialize(value) {
    if (!isValidCoordinateObject(value)) {
      throw new GraphQLError(`${JSON.stringify(value)} is not an valid location coordinates object.`);
    }

    return value;
  },

  parseValue(value) {
    if (!isValidCoordinateObject(value)) {
      throw new GraphQLError(`${JSON.stringify(value)} is not an valid location coordinates object.`);
    }

    return value;
  },

  parseLiteral(ast) {
    if (ast.kind !== Kind.OBJECT) {
      throw new GraphQLError(`Can only parse objects to coordinates but got a: ${ast.kind}`);
    }
    const location = {};

    ast.fields.forEach((field) => {
      const { value: name } = field.name;
      const { value } = field.value;

      location[name] = Number.parseFloat(value);
    });

    if (!isValidCoordinateObject(location)) {
      throw new GraphQLError(`${JSON.stringify(ast.value)} is not an valid location coordinates object.`);
    }

    return location;
  }
});

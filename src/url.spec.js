const { describe, it } = require('mocha');
const chai = require('chai');
const { Kind } = require('graphql/language');
const { GraphQLError } = require('graphql/error');
const url = require('./url');

const { expect } = chai;

describe('URL', () => {
  describe('validate URL', () => {
    it('should validate a correct URL ', () => {
      const imgURL = 'https://avatars1.githubusercontent.com/u/6735915?s=400&u=dd0a9abd499efc6316aa069464a9f147ca3e9171&v=4';
      expect(url.serialize(imgURL)).to.equal(imgURL);
    });

    it('should fail for invalid url', () => {
      expect(url.serialize.bind(url, 'xyz')).to.throw(TypeError);
    });
  });

  it('should parse literal', () => {
    const testURL = 'https://avatars1.githubusercontent.com/u/6735915?s=400&u=dd0a9abd499efc6316aa069464a9f147ca3e9171&v=4';
    expect(url.parseLiteral({
      value: testURL,
      kind: Kind.STRING
    })).to.equal(testURL);
  });

  it('should fail if parse  literal kind is not correct', () => {
    const testURL = 'https://avatars1.githubusercontent.com/u/6735915?s=400&u=dd0a9abd499efc6316aa069464a9f147ca3e9171&v=4';
    expect(url.parseLiteral.bind(url, {
      value: testURL,
      kind: Kind.INT
    })).to.throw(GraphQLError);
  });
});

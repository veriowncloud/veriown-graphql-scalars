const { GraphQLScalarType } = require('graphql');
const { GraphQLError } = require('graphql/error');
const { Kind } = require('graphql/language');
const { isEmail } = require('validator');

module.exports = new GraphQLScalarType({
  name: 'Email',

  description: 'A valid email.',

  serialize(value) {
    if (typeof value !== 'string') {
      throw new GraphQLError(`Value is not string: ${value}`);
    }

    // eslint-disable-next-line no-param-reassign
    value = value.trim();

    if (!isEmail(value)) {
      throw new GraphQLError(`Value is not a valid email: ${value}`);
    }

    return value;
  },

  parseValue(value) {
    if (typeof value !== 'string') {
      throw new GraphQLError(`Value is not string: ${value}`);
    }

    // eslint-disable-next-line no-param-reassign
    value = value.trim();

    if (!isEmail(value)) {
      throw new GraphQLError(`Value is not a valid email: ${value}`);
    }

    return value;
  },

  parseLiteral(ast) {
    if (ast.kind !== Kind.STRING) {
      throw new GraphQLError(`Can only validate strings as emails but got a: ${ast.kind}`);
    }

    // eslint-disable-next-line no-param-reassign
    ast.value = ast.value.trim();

    if (!isEmail(ast.value)) {
      throw new GraphQLError(`Value is not a valid email: ${ast.value}`);
    }

    return ast.value;
  }
});

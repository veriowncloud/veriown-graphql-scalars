const { describe, it } = require('mocha');
const chai = require('chai');
const { Kind } = require('graphql/language');
const { GraphQLError } = require('graphql/error');
const chance = require('chance').Chance();
const uuid = require('./uuid');

const { expect } = chai;

describe('GraphqlScalar: UUID', () => {
  describe('.serialize', () => {
    it('should validate a correct uuid ', () => {
      const value = chance.guid();

      expect(uuid.serialize(value)).to.equal(value);
    });

    it('should fail for invalid uuid', () => {
      expect(() => uuid.serialize('not-a-valid-uuid')).to.throw('not a valid UUID');
    });
  });

  describe('.parseValue', () => {
    it('should validate a correct uuid ', () => {
      const value = chance.guid();

      expect(uuid.parseValue(value)).to.equal(value);
    });

    it('should fail for invalid uuid', () => {
      expect(() => uuid.parseValue('not-a-valid-uuid')).to.throw('not a valid UUID');
    });
  });

  describe('.parseLiteral', () => {
    it('should parse literal for valid UUID', () => {
      const value = chance.guid();
      expect(uuid.parseLiteral({
        value,
        kind: Kind.STRING
      })).to.equal(value);
    });

    it('should fail if parse literal kind is not correct', () => {
      const value = chance.guid();
      expect(() => uuid.parseLiteral({
        value,
        kind: Kind.INT
      })).to.throw(GraphQLError);
    });

    it('should fail if parse literal value is not a valid UUID', () => {
      const value = 'not-a-valid-uuid';
      expect(() => uuid.parseLiteral({
        value,
        kind: Kind.STRING
      })).to.throw('not a valid UUID');
    });
  });
});

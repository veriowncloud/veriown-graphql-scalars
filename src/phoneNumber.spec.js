const { describe, it } = require('mocha');
const chai = require('chai');
const { Kind } = require('graphql/language');
const { GraphQLError } = require('graphql/error');
const phoneNumber = require('./phoneNumber');

const { expect } = chai;

describe('PhoneNumber', () => {
  describe('validate phone number', () => {
    it('should validate valid phone number ', () => {
      const phone = { countryCode: '+91', number: '9359999566' };
      expect(phoneNumber.serialize(phone)).to.deep.equal(phone);
    });

    it('should fail for invalid phone', () => {
      const phone = { countryCode: '+91', number: '93599995' };
      expect(phoneNumber.serialize.bind(phoneNumber, phone)).to.throw(GraphQLError);
    });
  });

  it('should parse literal', () => {
    const phone = { countryCode: '+91', number: '9359999566' };

    expect(phoneNumber.parseLiteral({
      fields: [
        {
          kind: Kind.OBJECT_FIELD,
          name: { kind: Kind.NAME, value: 'countryCode' },
          value: { kind: Kind.STRING, value: phone.countryCode }
        },
        {
          kind: Kind.OBJECT_FIELD,
          name: { kind: Kind.NAME, value: 'number' },
          value: { kind: Kind.STRING, value: phone.number }
        }
      ],
      kind: Kind.OBJECT
    })).to.deep.equal(phone);
  });

  it('should fail if parse  literal kind is not correct', () => {
    const phone = { countryCode: '91', number: '59999566' };
    expect(phoneNumber.parseLiteral.bind(phoneNumber, {
      value: phone,
      kind: Kind.FLOAT
    })).to.throw(GraphQLError);
  });
});
